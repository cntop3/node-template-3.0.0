 **使用gitpod远程服务器和polkadot.js.org连接substrate node节点进行交互 步骤说明** 

 **一、目标** 

    gitpod远程服务器具有很多优点 :   +1:  

1、Gitpod是一个在线IDE，github代码可以直接在gitpod远程服务器上运行。

2、gitpod远程服务器具有沙箱环境，不受本地配置影响，我们只需要关注代码自身的配置文件就可以。
（这里吐槽一下rust toolchain繁多的ninghtly版本  ）

3、可以连接polkadot.js.org/apps/  进行前端调试  :+1: 

---

 **二、具体步骤** 

1、在gitpod远程服务器运行github代码

Gitpod可以从任何GitHub页面启动。只需在任何GitHub-URL前加上“https://gitpod.io#”

例1：https://gitpod.io#https://github.com/PureStake/moonbeam 就直接启动了moonbeam master分支的代码了。

例2：https://gitpod.io#https://github.com/paritytech/substrate  就直接启动了substrate 代码了。

![在gitpod远程服务器运行github代码](https://pic4.zhimg.com/v2-2ef1df05f74cdb1f50a22aa33e96c987_r.jpg "在gitpod远程服务器运行github代码")




2、运行命令和参数（重要！！！）

2.1 安装rust toolchain等配置、 编译节点

系统自动安装配置文件中的rust toolchain版本号，（在本地配置繁多版本的同学，最知道这点的重要）

![系统自动安装配置文件中的rust toolchain版本](https://images.gitee.com/uploads/images/2021/1013/122414_4492782f_6547328.png "系统自动安装配置文件中的rust toolchain版本.png")




编译节点命令行：
`cargo build --release`

2.2 运行节点

    在Terminial窗口以下命令行启动node节点，

命令行为：

 `./target/release/moonbeam  --ws-external --rpc-cors all --dev`


    对其中的参数进行解释如下：
（1） --ws-external  允许外部ws连接，才能用https://polkadot.js.org/apps/连接节点
（2）--rpc-cors all   允许外部RPC连接
（3）--dev  开发模式运行节点 

弹出的窗口中，要点击 这个弹窗，记下9944端口的地址，设置wss地址时用到。

![点击打开窗口，查看地址](https://images.gitee.com/uploads/images/2021/1013/115732_ced26e25_6547328.png "9944.png")

![9944端口地址](https://images.gitee.com/uploads/images/2021/1013/112432_dec6ace8_6547328.png "9944端口地址.png")

3、连接https://polkadot.js.org/apps/ 进行代码调试

3.1 设置9944端口 为 public

在左侧 Remote Explorer 设置9944端口为public，方便wss连接。 
如图所示，点击9944 后面的 锁图标 就可以从private更改为public。

![把9944端口从private更改为public](https://images.gitee.com/uploads/images/2021/1013/120028_83520961_6547328.png "9944public.png")



3.2 设置wss地址

在 https://polkadot.js.org/apps/  中设置自定义终端。
wss地址，把https 改成wss，其他不变，填入，就可以连接了，等待链接成功。


![9944端口地址](https://images.gitee.com/uploads/images/2021/1013/112432_dec6ace8_6547328.png "9944端口地址.png")

如图上地址

`https://9944-tomato-cougar-2b8h73aq.ws-us18.gitpod.io/`

修改为 

`wss://9944-tomato-cougar-2b8h73aq.ws-us18.gitpod.io/`

自定义终端设置：

![自定义终端设置](https://images.gitee.com/uploads/images/2021/1013/113244_45709fa6_6547328.png "自定义终端.png")



Termial界面中也会有连接提示。


![wss连接反应](https://images.gitee.com/uploads/images/2021/1013/112729_67f1a3b7_6547328.png "wss连接反应.png")

---
    _连接成功！_   :star:  可以愉快的使用  https://polkadot.js.org/apps/  与节点交互了。   :star: 


 **三、修改建议** 

其他还有任何疑问，可以留言 交流

另外。gitpod远程代码运行的网址，3天之内只要经常打开，数据是可以保留的。

 **四、感谢** 

感谢Oneblock+ substrate3期进阶班 众多同学的热心帮助 特别是：雅珣、Emma 、Team1 郭助教、大宇、星释， 还有 ————。

