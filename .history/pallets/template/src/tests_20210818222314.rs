use crate::{mock::*, Error};
use frame_support::{assert_noop, assert_ok};
use super::*;

#[test]
fn create_claim_works() {
	new_test_ext().execute_with(|| {
		
		let claim= vec![0,1];
		assert_ok!(TemplateModule::create_claim(Origin::signed(1), claim.clone()));
		assert_eq!(
			Proofs::<Test>::get(&claim), 
			Some{}(1, frame_system::pallet::<Test>::block_number())))
		
	})
}


