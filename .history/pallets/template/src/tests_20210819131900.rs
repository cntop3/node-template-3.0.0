use crate::{mock::*, Error};
use frame_support::{assert_noop, assert_ok};
use super::*;

#[test]
fn create_claim_works() {
	new_test_ext().execute_with(|| {
	// Dispatch a signed extrinsic.引入测试用外部事件
		let claim = vec![0,1];
		assert_ok!(TemplateModule::create_claim(Origin::signed(1), claim.clone()));
		// assert.ok(value)这个函数的用法没查到。
		assert_eq!(
		// 查到断言函数 assert_eq!(a, b); 比较a,b两个值是否相等。
			Proofs::<Test>::get(&claim), 
            // 教程演示代码需要修改：去掉some，2号大宇说因为函数的返回值不是option类型
			(1, frame_system::Pallet::<Test>::block_number()));
		
	})
}

#[test]
fn create_claim_failed_when_claim_already_exist(){
	new_test_ext().execute_with(|| {
		let claim = vec![0,1];
		let _ = (TemplateModule::create_claim(Origin::signed(1), claim.clone()));
			assert_noop!(
			TemplateModule::create_claim(Origin::signed(1), claim.clone()),
		// 教程演示代码需要修改：ProofAlreadyExist可能是2021-05版的写法。
        Error::<Test>::ProofAlreadyClaimed
	);
	})
}


#[test]
fn revoke_claim_works(){
	new_test_ext().execute_with(||{
		let claim = vec![0,1];
		let _ = (TemplateModule::create_claim(Origin::signed(1), claim.clone()));

		assert_ok!(TemplateModule::revoke_claim(Origin::signed(1), claim.clone()));
		assert_eq!(
			Proofs::<Test>::get(&claim),
            // 教程演示代码需要修改：12号星释说，storage那，最后一个配置，如果是 ValueQuery， 
			//那get返回的不是Option类型，原始类型，那这返回的就是u64, 这里试下0行不行，
			//不然就用contains_key 替代 get，返回 false/true
			(0, 0)
		);

	})
}


#[test]
fn revoke_claim_failed_when_claim_is_not_exist(){
	new_test_ext().execute_with(|| {
		let claim = vec![0,1];
		let _ = (TemplateModule::revoke_claim(Origin::signed(1), claim.clone()));
			assert_noop!(
			TemplateModule::revoke_claim(Origin::signed(1), claim.clone()),
        // 教程演示代码需要修改：NoSuchProof是2021-08版的写法。
			Error::<Test>::NoSuchProof
	);
	})
}

// 作业：转移存证的测试用例
