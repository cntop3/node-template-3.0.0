use crate::{mock::*, Error};
use frame_support::{assert_noop, assert_ok};

#[test]
fn create_claim_works() {
	new_test_ext().execute_with(|| {
		
		let claim= vec![0,1];
		assert_ok!(TemplateModule::create_claim(Origin::signed(1), claim.clone()));
		assert_eq!(
			Proofs::<test>::get(&claim),
			Some(1,frame_system::pallet::<Test>::block_number())
		);
	})	
}

#[test]
fn create_claim_failed_when_claim_already_exist(){
	new_test_ext().execute_with(|| {
		let claim = vec![0,1]
		let _ =
	}
}
