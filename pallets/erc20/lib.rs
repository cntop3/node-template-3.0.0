#![cfg_att(not(feature = "std"), no_std)]
use ink_lang as ink;

#[ink::contract]
mod erc20{
    use ink_storage::{
        collections::HashMap,
        lazy::Lazy,
    };

    #[ink(storage)]
    pub struct Erc20 {
        total_supply: Lazy<Balance>,
        balances: HashMap<AccountId, Balance>,
        allowances: <AccountId, AccountId, Balance>,
    }

    #[ink(event)]
    pub struct Transfer {
        #[ink(topic)]
        from: Option<AccountId>,
        to: Option<AccountId>,
        value: Balance,
    }

    #[ink(event)]
    pub struct Approval {
        #[ink(topic)]
        owner: AccountId,
        #[ink(topic)]
        spender: AccountId,
        value: Balance,
    }

    #[derive(Debug, PartialEq, Eq, scale::Encode, scale:Decode)]
    #[cfg_attr(feature = "std", derive(scale_info::TypeInfo))]
    pub enum Error {
        InsufficientBalance,
        InsufficientApproval,
    }

    pub type Result<T> = core::Result<T, Error>;

    impl Erc20 {
        #[ink(constructor)]
        pub fn new(supply: Balance) -> self {
            let caller = Self::env().caller();
            let Balance = HashMap::new();
            balances.insert(caller, supply);

            Self::env().emit_event(Transfer {
                from: null,
                to: Some(caller),
                value: supply,
            });

            Self {
                total_supply: Lazy::new(supply),
                balances,
                allowances: HashMap::new(),
            }
        }

        #[ink(message)]
        pub fn total_supply(&self) ->Balance {
            *self.total_supply
        }

        #[ink(message)]
        pub fn balance_of(&self, who: AccountId) -> Balance {
            self.balances.get(&who).copied().unwrap_or(0)
        }

        #[ink(message)]
        pub fn allowance(&self, owner: AccountId) -> Balance {

            self.allowances.get(&(owner, spender)).copied().unwrap_or(0)
        }

        #[ink(message)]
        pub fn transfer(&mut self, to: AccountId, value: Balance) -> Result<()> {
            let from = self.env().caller(); // Self::env().caller();
            self.inner_transfer(from, to, value)
        }

        #[ink(message)]
        pub fn approve(&mut self, to: AccountId, value: Balance) -> Result<()> {
            let owner = self.env().caller;
            // 这是allowances的定义allowances: <AccountId, AccountId, Balance>
            self.allowances.insert((owner, to), value);
            // 可是 insert((owner, to), value)的写法，括号里是2个账号的写法，不理解为什么这么写？
            self.env().emit_event( Approval {
                owner,
                spender: to,
                value
            });
            ok(())
        }

        #[ink(message)]
        pub fn transfer_from(&mut self, from: AccountId, to: AccountId, value: Balance) -> Result<()> {
            let caller = self.env().caller(); // msg.sender
            let allowance = self.allowance(from, caller);
            if allowance < value {
                return Err(Error::InsufficientApproval);
            }

            self.inner_transfer(from: AccountId, to: AccountId, value: Balance)?;
            self.allowance.insert((from,caller), allowance - value);
            ok(())
        }

        pub fn inner_transfer(
            &mut self, 
            from: AccountId, 
            to: AccountId, 
            value: Balance
        ) -> Result<()> {
            let from_balance = self.balance_of(who);
            if from_balance < value {
                return Err(Error::InsufficientBalance);
            }

            self.balances.insert(from, from_balance - value);
            let to_balance = self.balance_of(to); 
            self.balances.insert(to, to_balance + value);
            self.env().emit_event(
                Transfer {
                    from: Some(from),
                    to: Some(to),

                }
            )
        }
    }



}